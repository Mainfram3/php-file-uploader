<?php

// initial variables
$target_dir = 'uploads/';
$target = $target_dir . basename($_FILES['fileUpload']['name']); //uploadstargetfile
$uploadOk = 1;
$img_file_type = strtolower(pathinfo($target, PATHINFO_EXTENSION)); // We need the image file type to test if the submitted image is ok later on 

//check if file is an actual image
if (isset($_POST['submit']))
{
	$check = getimagesize($_FILES['fileUpload']['tmp_name']); //tmp_name is the temp name after submit
	if ($check !== false )
	{
		echo 'file is an image';
		$uploadOk = 1;
	} else {
		die('file is not an image');
		$uploadOk = 0;
	}
}

// Check if file already exists
if (file_exists($target))
{
	die('File already exists');
	$uploadOk = 0;
}

//Limit image size
if ($_FILES['fileUpload']['size'] > 500000) 
{
	die('File is to big');
	$uploadOk = 0;
}

// allow only certain inge file types. This is the one where we create an array of accepted extension types then check it with pathinfo()
$allowed = array(
	'png'  => 'image/png',
	'jpg'  => 'image/jpg',
	'jpeg' => 'image/jpeg'
);

if (!array_key_exists($img_file_type, $allowed)) //(key, array)
{
	die('Image has to be .png, .jpg, or .jpeg');
	$uploadOk = 0;
}

// Check if $uploadOk is set to 0 by error
if ($uploadOk == 0)
{
	echo 'Sorry but something appears to be wrong and your file was not uploaded.';
// If everything is ok try and upload the file
} else {
	if (move_uploaded_file($_FILES['fileUpload']['tmp_name'], $target))
	{
		echo 'The file ' . basename($_FILES['fileUpload']['name']) . ' has been uploaded';
    } else {
    	echo 'Your file was not uploaded, time to check the logs';
	}
}


?>