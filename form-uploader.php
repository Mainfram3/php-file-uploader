<?php include 'templates/header.php'; ?>

<br>

<form action="uploader.php" method="post" enctype="multipart/form-data">
	<h2>Upload an image</h2>
	<label for="theFile">Filename:</label>
	<input type="file" name="fileUpload" id="theFile">
	<input type="submit" name="submit" value="Submit" id="submit"> <!-- id is strictly for css -->
	<p><strong>Note:</strong> Only .jpg .png and .jpeg files allowed</p>
</form>

<?php include 'templates/footer.php'; ?>